from .base import Base
from .utils.checks import checkIsRGB, checkInBounds


class Color(Base):

    def __init__(self, id: str, red: int, green: int, blue: int, alpha: int, usage: list, since: str, **kwargs):
        checkIsRGB(red, **{'attribute': 'red value'})
        self.red = red
        checkIsRGB(green, **{'attribute': 'green value'})
        self.green = green
        checkIsRGB(blue, **{'attribute': 'blue value'})
        self.blue = blue
        checkInBounds(alpha, 0, 100, **{'attribute': 'alpha value'})
        self.alpha = alpha
        self.usage = usage
        super().__init__(id, since)
