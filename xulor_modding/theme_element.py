from .base import Base


class ThemeElement(Base):

    def __init__(self, id: str, scaled: bool, specificPixmaps: list, childrenThemeElements: list, since: str, **kwargs):
        self.scaled = scaled
        self.specificPixmaps = specificPixmaps
        self.childrenThemeElements = childrenThemeElements
        super().__init__(id, since)
