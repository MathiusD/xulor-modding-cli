from .base import Base


class Texture(Base):

    def __init__(self, id: str, path: str, overridable: bool, since: str, **kwargs):
        self.path = path
        self.overridable = overridable
        super().__init__(id, since)

    @property
    def getUri(self):
        return "https://wakfu.cdn.ankama.com/gamedata/theme/images/%s.png" % self.id
