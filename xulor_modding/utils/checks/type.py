from .defined import checkIsDefined


def checkIsGoodType(value, typeExpected, **kwargs):
    checkIsDefined(value, **kwargs)
    if type(value) is not typeExpected:
        raise AttributeError("%s must be an integer" %
                             kwargs.get('attribute', 'value'))
