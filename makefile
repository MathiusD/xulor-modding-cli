ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

pip:
	@pip3 install -r requirements.txt

run: pip
	@python3 -m xulor_modding -h

pipTesting: pip
	@pip3 install -r tests-requirements.txt

tests: pipTesting unittest show_reports

unittest:
	@python3 -m xmlrunner -v -o=reports

coverage:
	@rm -f .coverage
	@coverage run -m unittest

coverage_report: coverage
	@coverage report --include=xulor_modding/*.py
	@coverage xml --include=xulor_modding/*.py -o reports/coverageReport.xml
	@coverage html --include=xulor_modding/*.py -d reports/coverage

mutmut: coverage
	@rm -f .mutmut-cache
	-@mutmut run --use-coverage

mutmut_report: mutmut
	@mutmut junitxml >> reports/mutmutReport.xml
	@mutmut html
	@rm -rf reports/mutmut
	@mv html reports/mutmut

show_reports: coverage_report mutmut_report
	@echo "Coverage report generated at file://$(ROOT_DIR)/reports/coverage/index.html"
	@echo "Mutation report generated at file://$(ROOT_DIR)/reports/mutmut/index.html"

clearTests:
	@rm -rf reports