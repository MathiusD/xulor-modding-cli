from unittest import TestCase

from parameterized import parameterized
from xulor_modding.texture import Texture


class TestTexture(TestCase):

    @parameterized.expand([
        ("foo", "example", False, "Example"),
        ("themeSimple", "theme/images/themeSimple.tga", True, "10074000"),
        (None, None, None, None)
    ])
    def testInit(self, id: str, path: str, overridable: bool, since: str):
        texture = Texture(id, path, overridable, since)
        self.assertEqual(texture.id, id)
        self.assertEqual(texture.path, path)
        self.assertEqual(texture.overridable, overridable)
        self.assertEqual(texture.since, since)

    @parameterized.expand([
        (Texture("foo", "example", False, "Example"),
         "https://wakfu.cdn.ankama.com/gamedata/theme/images/foo.png"),
        (Texture("themeSimple", "theme/images/themeSimple.tga", True, "10074000"),
         "https://wakfu.cdn.ankama.com/gamedata/theme/images/themeSimple.png")
    ])
    def testGetUri(self, data: Texture, expected: str):
        self.assertEqual(data.getUri, expected)
