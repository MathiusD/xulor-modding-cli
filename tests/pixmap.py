from unittest import TestCase
from unittest.mock import MagicMock

from parameterized import parameterized
from xulor_modding.pixmap import Pixmap


class TestPixmap(TestCase):

    @parameterized.expand([
        ("foo", MagicMock(name='texture'), 'space',
         MagicMock(name='coordinate'), MagicMock(name='size'), False, True, "Example"),
        ("themeSimple", MagicMock(name='texture'), 'space',
         MagicMock(name='coordinate'), MagicMock(name='size'), True, False,  "10074000"),
        (None, None, None, None, None, None, None, None)
    ])
    def testInit(self, id: str, texture, position: str, coordinate, size,
                 flipHorizontally: bool, flipVertically: bool, since: str):
        pixmap = Pixmap(id, texture, position, coordinate, size,
                        flipHorizontally, flipVertically, since)
        self.assertEqual(pixmap.id, id)
        self.assertEqual(pixmap.texture, texture)
        self.assertEqual(pixmap.position, position)
        self.assertEqual(pixmap.coordinate, coordinate)
        self.assertEqual(pixmap.size, size)
        self.assertEqual(pixmap.flipHorizontally, flipHorizontally)
        self.assertEqual(pixmap.flipVertically, flipVertically)
        self.assertEqual(pixmap.since, since)
