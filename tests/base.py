from unittest import TestCase

from parameterized import parameterized
from xulor_modding.base import Base


class TestBase(TestCase):

    @parameterized.expand([
        ("foo", "Example"),
        ("themeSimple", "10074000"),
        (None, None)
    ])
    def testInit(self, id: str, since: str):
        base = Base(id, since)
        self.assertEqual(base.id, id)
        self.assertEqual(base.since, since)
