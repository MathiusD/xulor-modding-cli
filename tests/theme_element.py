from unittest import TestCase
from unittest.mock import MagicMock

from parameterized import parameterized
from xulor_modding.theme_element import ThemeElement


class TestThemeElement(TestCase):

    @parameterized.expand([
        ("foo", True, [], [], "Example"),
        ("themeSimple", False, [MagicMock(name="pixmap")], [
         ThemeElement("foo", True, [], [], "idk")], "10074000"),
        (None, None, None, None, None)
    ])
    def testColor(self, id: str, scaled: bool, specificPixmaps: list, childrenThemeElements: list, since: str):
        themeElement = ThemeElement(
            id, scaled, specificPixmaps, childrenThemeElements, since)
        self.assertEqual(themeElement.id, id)
        self.assertEqual(themeElement.scaled, scaled)
        self.assertEqual(themeElement.specificPixmaps, specificPixmaps)
        self.assertEqual(themeElement.childrenThemeElements,
                         childrenThemeElements)
        self.assertEqual(themeElement.since, since)
