from unittest import TestCase

from parameterized import parameterized
from xulor_modding.coordinate import Coordinate


class TestCoordinate(TestCase):

    @parameterized.expand([
        (1, 5),
        (-25, 65),
        (None, None)
    ])
    def testInit(self, x: int, y: int):
        coordinate = Coordinate(x, y)
        self.assertEqual(coordinate.x, x)
        self.assertEqual(coordinate.y, y)
