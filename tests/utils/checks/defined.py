from unittest import TestCase

from parameterized import parameterized
from xulor_modding.utils.checks.defined import checkIsDefined


class TestDefined(TestCase):

    @parameterized.expand([
        (12, ),
        (12, {'attribute': 'example'}),
        (12, {'other': 'example'}),
        ('ex',),
        ('ex', {'attribute': 'example'}),
        ('ex', {'other': 'example'}),
        (None, {}, AttributeError("value must be defined")),
        (None, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, {'other': 'example'}, AttributeError(
            "value must be defined"))
    ])
    def testCheckRGB(self, value, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkIsDefined(value, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkIsDefined(value, **kwargs)
