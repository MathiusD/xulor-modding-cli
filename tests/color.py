from unittest import TestCase

from parameterized import parameterized
from xulor_modding.color import Color


class TestColor(TestCase):

    @parameterized.expand([
        ("foo", 1, 2, 3, 4, [], "Example"),
        ("themeSimple", 12, 12, 12, 100, ["foo"], "10074000"),
        (None, None, None, None, None, None, None,
         AttributeError("red value must be defined")),
        (None, 12, None, None, None, None, None,
         AttributeError("green value must be defined")),
        (None, 12, 12, None, None, None, None,
         AttributeError("blue value must be defined")),
        (None, 12, 12, 12, None, None, None,
         AttributeError("alpha value must be defined")),
        (None, 12, 12, 12, -1, None, None,
         AttributeError("alpha value must be in following bounds (0-100)")),
        (None, 12, 12, 12, 101, None, None,
         AttributeError("alpha value must be in following bounds (0-100)"))
    ])
    def testColor(self, id: str, red: int, green: int, blue: int, alpha: int, usage: list, since: str, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                Color(id, red, green, blue, alpha, usage, since)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            color = Color(id, red, green, blue, alpha, usage, since)
            self.assertEqual(color.id, id)
            self.assertEqual(color.red, red)
            self.assertEqual(color.green, green)
            self.assertEqual(color.blue, blue)
            self.assertEqual(color.alpha, alpha)
            self.assertEqual(color.usage, usage)
            self.assertEqual(color.since, since)
