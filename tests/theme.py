from unittest import TestCase
from unittest.mock import MagicMock

import requests
from parameterized import parameterized
from xulor_modding.theme import Theme


class TestTheme(TestCase):

    @parameterized.expand([
        ([], [], [], []),
        ([MagicMock(name="textures")], [MagicMock(name="pixmap")], [
         MagicMock(name="colors")], [MagicMock(name="themeElements")]),
        (None, None, None, None)
    ])
    def testInit(self, textures: list, pixmaps: list, colors: list, themeElements: list):
        theme = Theme(textures, pixmaps, colors, themeElements)
        self.assertEqual(theme.textures, textures)
        self.assertEqual(theme.pixmaps, pixmaps)
        self.assertEqual(theme.colors, colors)
        self.assertEqual(theme.themeElements, themeElements)

    class ResponseMock:

        def __init__(self, status_code: int, json: dict = None):
            self.status_code = status_code
            self._json = json

        def json(self):
            return self._json

    @parameterized.expand([
        (200, {'Example': True}),
        (500,),
        (400,)
    ])
    def testFetchLastTheme(self, fetchResultCode: int, fetchJson: dict = None):
        response = self.ResponseMock(fetchResultCode, fetchJson)
        requests.get = MagicMock(side_effect=[response])
        if fetchResultCode == 200:
            self.assertEqual(Theme.fetchLastTheme(), fetchJson)
        else:
            with self.assertRaises(Exception) as error:
                Theme.fetchLastTheme()
            self.assertEqual(error.exception.args,
                             ("Error on fetch of latest theme",))
        requests.get.assert_called_with(
            "https://wakfu.cdn.ankama.com/gamedata/theme/theme.json")
